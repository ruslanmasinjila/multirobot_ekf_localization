%   plots various types of graphs and displays information on terminal

function makePlots( robots, numRuns )

%   The ANEES can have outliers which could potentially distort the
%   appearance of its curves. Keep outlierThreshold 1000 for
%   simulationLength<=50. Adjust as necessary.
outlierThreshold=25;

%##########################################################################
% use the basic colors for the first 6 graphs

randomColor(1,:)=[1,0,0];
randomColor(2,:)=[0,0,0];
randomColor(3,:)=[0,0,1];
randomColor(4,:)=[0,0.5,0.5];
randomColor(5,:)=[1,0,1];
randomColor(6,:)=[1,1,0];
      
 %  generate random colors for the rest of the graphs
 for i=(length(randomColor(:,1))+1):length(robots)
     
     randomColor(i,:)=[rand,rand,rand];   
 end

 %##########################################################################

% Ground Truths and EKF 
figure;
set(gca,'fontsize',30);

for i=1:length(robots)
    
 
plot(robots(i).groundTruth(:,1),robots(i).groundTruth(:,2),'color',randomColor(i,:),'LineWidth',5);
hold on;

plot(robots(i).groundTruth(1,1),robots(i).groundTruth(1,2),'*','MarkerSize',20,'color',randomColor(i,:));
hold on;

text(robots(i).groundTruth(1,1),robots(i).groundTruth(1,2),['R',num2str(i)],'fontsize',30,'fontweight','bold','color',randomColor(i,:));
hold on;

% plot(robots(i).mu(:,1),robots(i).mu(:,2),'--','color',randomColor(i,:),'LineWidth',2);
% hold on;

end

title(['GROUND   TRUTH   TRAJECTORIES   OF  ',num2str(length(robots)),'   ROBOTS'],'fontsize',30);
xlabel('x (m)','fontsize',30);
ylabel('y (m)','fontsize',30);


%##########################################################################

% Ground Truths and encoder only
figure;
set(gca,'fontsize',30);

for i=1:length(robots)
    
plot(robots(i).groundTruth(:,1),robots(i).groundTruth(:,2),'color',randomColor(i,:));
hold on;

plot(robots(i).groundTruth(1,1),robots(i).groundTruth(1,2),'*','MarkerSize',20,'color',randomColor(i,:));
hold on;

text(robots(i).groundTruth(1,1),robots(i).groundTruth(1,2),['R',num2str(i)],'fontsize',30,'fontweight','bold');
hold on;

plot(robots(i).encoderPose(:,1),robots(i).encoderPose(:,2),'--','color',randomColor(i,:),'LineWidth',2);
hold on;

end

title(['Ground Truths (solid) and Encoder Estimates (broken) of ',num2str(length(robots)),' Robots'],'fontsize',30);
xlabel('x (m)','fontsize',30);
ylabel('y (m)','fontsize',30);

%##########################################################################

% Average Normalized Estimation Error Squared
figure;
set(gca,'fontsize',30);

%   draw the upper and lower anees bounds
[lower_bound,upper_bound] = anees_bounds(numRuns);


for i=1:length(robots)
    
%   remove outliers in anees
indices = robots(i).anees>outlierThreshold;
robots(i).anees(indices) = [];

%   make a copy of robots(i).actualDistanceTraveled.
%   resize the copy to match the dimensions of the robots(i).anees vector

actualDistanceTraveledANEES=robots(i).actualDistanceTraveled;
actualDistanceTraveledANEES(indices)=[];
    
plot(actualDistanceTraveledANEES,upper_bound.*ones(1,length(actualDistanceTraveledANEES)),'--k');
hold on;

plot(actualDistanceTraveledANEES,robots(i).anees,'color',randomColor(i,:),'LineWidth',2);
hold on;

plot(actualDistanceTraveledANEES,lower_bound.*ones(1,length(actualDistanceTraveledANEES)),'--k');
hold on;

% text(actualDistanceTraveledANEES(end),robots(i).anees(end),['R',num2str(i)],'fontsize',30,'fontweight','bold');
% hold on;


end

title(['ANEES   OF  ',num2str(length(robots)),'  ROBOTS   FOR   TUNED   EKF   (A=15)'],'fontsize',30);
xlabel('Distance Moved by Robot (m)','fontsize',30);
ylabel(['ANEES for ',num2str(numRuns),' simulation runs'],'fontsize',30);

%##########################################################################

% absolute errors in x coordinates
figure;
set(gca,'fontsize',30);

for i=1:length(robots)
    
 
plot(robots(i).actualDistanceTraveled,robots(i).actualError(:,1),'color',randomColor(i,:),'LineWidth',2);
hold on;

text(robots(i).actualDistanceTraveled(end),robots(i).actualError(end,1),['R',num2str(i)],'fontsize',30,'fontweight','bold');
hold on;

end

title(['Actual Errors in X-Axis of ',num2str(length(robots)),' Robots'],'fontsize',30);
xlabel('Distance Moved by Robot','fontsize',30);
ylabel('Error (m)','fontsize',30);

%##########################################################################

% absolute errors in y coordinates
figure;
set(gca,'fontsize',30);

for i=1:length(robots)
    
 
plot(robots(i).actualDistanceTraveled,robots(i).actualError(:,2),'color',randomColor(i,:),'LineWidth',2);
hold on;

text(robots(i).actualDistanceTraveled(end),robots(i).actualError(end,2),['R',num2str(i)],'fontsize',30,'fontweight','bold');
hold on;

end

title(['Actual Errors in Y-Axis of ',num2str(length(robots)),' Robots'],'fontsize',30);
xlabel('Distance Moved by Robot (m)','fontsize',30);
ylabel('Error (m)','fontsize',30);

%##########################################################################

end

