%   refines the pose estimated poses using relative measurements.

function movingRobot=correctPoseEstimates(movingRobot,stationaryRobot)

%##########################################################################
%   EKF Tuning parameters
Cii=15*length(stationaryRobot.ekfDistanceCombined);
%Cii=1;


%##########################################################################

%   get relative pose estimates and measurements.
movingRobot = estimateRelativePose(movingRobot,stationaryRobot);

%##########################################################################

%   estimate the partial derivative of the observation/measurement w.r.t to
%   predicted state (priori)
[Hr, Hl]=evaluateMeasurementJacobians(movingRobot.mu_bar,stationaryRobot.mu(end,:));

%##########################################################################

%   Determine the covariance matrix of the sensors
Qt=getSensorCovariance([movingRobot.sigma_rho,movingRobot.sigma_phi]);

%##########################################################################

Sr=(Hr)*(movingRobot.sigma_bar)*(Hr');
Sl=Cii*(Hl)*stationaryRobot.sigma{end}*(Hl');

%   calculate innovation covariance
S=  Sr+Sl+Qt;

%##########################################################################    

%   Calculate Kalman Gain (Kgain)
Kgain=  ((movingRobot.sigma_bar)*(Hr'))/S;   

%##########################################################################

%   Update mean for moving Robot
movingRobot.mu(end+1,:)=movingRobot.mu_bar+(Kgain*movingRobot.Z_diff)'; 

%##########################################################################

%   Normalize theta to [-pi,pi]
movingRobot.mu(end,3)=normalizeAngle(movingRobot.mu(end,3));

%##########################################################################

%   Update Covariance for moving Robot
movingRobot.sigma{end+1}=(eye(3,3)-Kgain*Hr)*movingRobot.sigma_bar;

%##########################################################################
%   increment the ekf distances travelled by the robot
movingRobot.ekfDistanceCombined=...
    movingRobot.ekfDistanceCombined+...
        sqrt((movingRobot.mu(end,1)-movingRobot.mu(end-1,1))^2+...
            (movingRobot.mu(end,2)-movingRobot.mu(end-1,2))^2);
        
movingRobot.ekfDistanceX=...
    movingRobot.ekfDistanceX+...
        abs(movingRobot.mu(end,1)-movingRobot.mu(end-1,1));
    
movingRobot.ekfDistanceY=...
    movingRobot.ekfDistanceY+...
        abs(movingRobot.mu(end,2)-movingRobot.mu(end-1,2));
        
    
    

end